import 'package:bookshop_porject/screen/cart_screen.dart';
import 'package:bookshop_porject/widgets/book_card.dart';
import 'package:bookshop_porject/widgets/book_card_primary.dart';
import 'package:bookshop_porject/screen/book_detail.dart';
import 'package:bookshop_porject/model/book.dart';
import 'package:bookshop_porject/fade/fade_in_animation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<Products>(context);
    final pdts = productData.items;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: const Text(
          "Anime MangaStore",
          style: TextStyle(
            fontWeight: FontWeight.w300,
            color: Colors.black,
          ),
        ),
        centerTitle: true,
        leading: const Icon(
          Icons.menu,
          color: Colors.black54,
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.only(left: 17.0),
        child: Column(
          children: [
            FadeInAnimation(
                delay: 1,
                child: Image.asset("images/pic1.png", fit: BoxFit.contain)),
            const SizedBox(height: 30.0),
            FadeInAnimation(
              delay: 2,
              child: Padding(
                padding: const EdgeInsets.only(right: 17.0, bottom: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "Popular books",
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "view all",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.black38,
                      ),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              width: double.infinity,
              height: 230.0,
              child: ListView.builder(
                itemCount: pdts.length,
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  var book = pdts[index];
                  return FadeInAnimation(
                    delay: 2,
                    child: InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => BookDetail(
                                  book: book,
                                ),
                              ));
                        },
                        child: BookCardPrimary(book: book)),
                  );
                },
              ),
            ),
            const SizedBox(height: 15.0),
            FadeInAnimation(
              delay: 3,
              child: Padding(
                padding: const EdgeInsets.only(right: 17.0, bottom: 15.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: const [
                    Text(
                      "You may also like",
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      "view",
                      style: TextStyle(
                        fontSize: 15.0,
                        fontWeight: FontWeight.w500,
                        color: Colors.black38,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 15.0),
            ListView.builder(
              itemCount: pdts.length,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              physics: const ScrollPhysics(),
              itemBuilder: (context, index) {
                var book = pdts[index];
                return FadeInAnimation(
                  delay: 3,
                  child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => BookDetail(
                                book: book,
                              ),
                            ));
                      },
                      child: BookCardWidget(book: book)),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
