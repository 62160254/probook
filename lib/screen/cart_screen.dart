import 'package:bookshop_porject/model/book.dart';
import 'package:flutter/material.dart';
import '../widgets/book_card.dart';
import 'package:provider/provider.dart';
import 'package:bookshop_porject/model/cart.dart';
import 'package:bookshop_porject/widgets/cart_bookitem.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'My Cart',
          style: TextStyle(fontSize: 25, color: Colors.black87),
        ),
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
                itemCount: cart.items.length,
                itemBuilder: (ctx, i) => CartPdt(
                    cart.items.values.toList()[i].id,
                    cart.items.keys.toList()[i],
                    cart.items.values.toList()[i].price,
                    cart.items.values.toList()[i].quantity,
                    cart.items.values.toList()[i].name)),
          ),
          // CheckoutButton(
          //   cart: cart,
          // ),
        ],
      ),
    );
  }
}



// class CheckoutButton extends StatefulWidget {
//   final Cart cart;

//   const CheckoutButton({required this.cart});
//   @override
//   _CheckoutButtonState createState() => _CheckoutButtonState();
// }

// class _CheckoutButtonState extends State<CheckoutButton> {
//   @override
//   Widget build(BuildContext context) {
//     return FlatButton(
//       child: Text('Checkout'),
//       onPressed: widget.cart.totalAmount <= 0
//           ? null
//           : () async {
//               await Provider.of<Orders>(context, listen: false).addOrder(
//                   widget.cart.items.values.toList(), widget.cart.totalAmount);
//               widget.cart.clear();
//             },
//     );
//   }
// }
