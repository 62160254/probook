import 'package:flutter/foundation.dart';

class Book with ChangeNotifier {
  final String id;
  final String name;
  final double price;
  final String urlimage;
  final String description;
  Book(
      {required this.id,
      required this.name,
      required this.price,
      required this.urlimage,
      required this.description});

  
}

class Products with ChangeNotifier {
  List<Book> dummyBook = [
    Book(
      id: "1",
      name: "One Piece Wanokuni",
      price: 15.00,
      urlimage: "images/book1.jpg",
      description:
          "The Ninja-Pirate-Mink-Samurai Alliance gather in Wano Country and prepare for the battle against the Beasts Pirates. The history of Kozuki Oden and his ties to Wano Country, Whitebeard and Gol D. Roger are revealed, and the Alliance assaults Onigashima to take down the allied forces of Kaidou and Big Mom, all while alliances shift on all sides of the siege. Meanwhile, after the events during the recent Levely, the world goes through dramatic changes.",
    ),
    Book(
      id: "2",
      name: "Shingeki no Kyojin: The Final Season Part 2",
      price: 4.55,
      urlimage: "images/book2.jpg",
      description:
          "The war for Paradis zeroes in on Shiganshina just as Jaegerists have seized control. After taking a huge blow from a surprise attack led by Eren, Marley swiftly acts to return the favor. With Zeke’s true plan revealed and a military forced under new rule, this battle might be fought on two fronts. Does Eren intend on fulfilling his half-brother’s wishes or does he have a plan of his own?",
    ),
    Book(
      id: "3",
      name: "Shikkakumon no Saikyou Kenja",
      price: 3.44,
      urlimage: "images/book3.jpg",
      description:
          "At birth, mages randomly acquire one of the four \"crests\" that represents the extent of their magical capability. Equipped with a crest specializing in creation, a man named Gaius reached the ceiling of his potential, becoming known as the world's strongest sage. Despite his overwhelming power, he is unsatisfied with his abilities and desires to possess the mark suitable for close combat. Knowing that a person's crest is unchangeable, Gaius decides to reincarnate far into the future, hoping to alter his fate.",
    ),
    Book(
      id: "4",
      name: "Mahouka Koukou no Rettousei Raihousha-hen",
      price: 7.43,
      urlimage: "images/book4.jpg",
      description:
          "Following the events of \"Scorched Halloween\", the world is introduced to a terrifyingly powerful Strategic class magician. In an effort to uncover the identity of this person, the United States of the North American Continent (USNA) dispatches the most powerful asset in its arsenal to Japan on a covert mission—the elite magician unit \"Stars\" and its commander, Angie Sirius.",
    ),
    Book(
      id: "5",
      name: "Tensei shitara Slime Datta Ken 2",
      price: 4.55,
      urlimage: "images/book5.jpg",
      description:
          "Taking a break from his time as a teacher, the powerful slime Rimuru Tempest returns to his kingdom, eponymously named Tempest, just in time to begin negotiations with a nearby nation—the Kingdom of Eurazania. While the negotiations are anything but peaceful, they do end successfully, allowing Rimuru to return and finish teaching. When trying to again return to Tempest, this time permanently, Rimuru is stopped by a mysterious figure who is somehow able to constrain the many magical abilities he has at his disposal.",
    ),
    Book(
      id: "6",
      name: "Horimiya",
      price: 5.0,
      urlimage: "images/book6.jpg",
      description:
          "Although admired at school for her amiability and academic prowess, high school student Kyouko Hori has been hiding another side of her. With her parents often away from home due to work, Hori has to look after her younger brother and do the housework, leaving no chance to socialize away from school.",
    ),
    Book(
      id: "7",
      name: "Kimetsu no Yaiba: Yuukaku-hen",
      price: 3.56,
      urlimage: "images/book7.jpg",
      description:
          "The devastation of the Mugen Train incident still weighs heavily on the members of the Demon Slayer Corps. Despite being given time to recover, life must go on, as the wicked never sleep: a vicious demon is terrorizing the alluring women of the Yoshiwara Entertainment District. The Sound Pillar, Tengen Uzui, and his three wives are on the case. However, when he soon loses contact with his spouses, Tengen fears the worst and enlists the help of Tanjirou Kamado, Zenitsu Agatsuma, and Inosuke Hashibira to infiltrate the district's most prominent houses and locate the depraved Upper Rank demon.",
    ),
    Book(
      id: "8",
      name: "Vanitas no Carte",
      price: 6.53,
      urlimage: "images/book8.jpg",
      description:
          "Scorned by others of his kind for being born under a blue moon, the vampire Vanitas grew afraid and desolate. According to legend, he created a cursed grimoire known as the \"Book of Vanitas\", and it is said he would one day use it to bring retribution upon all vampires of the crimson moon.",
    ),
    Book(
      id: "9",
      name: "Nanatsu no Taizai",
      price: 4.05,
      urlimage: "images/book9.jpg",
      description:
          "In a world similar to the European Middle Ages, the feared yet revered Holy Knights of Britannia use immensely powerful magic to protect the region of Britannia and its kingdoms. However, a small subset of the Knights supposedly betrayed their homeland and turned their blades against their comrades in an attempt to overthrow the ruler of Liones. They were defeated by the Holy Knights, but rumors continued to persist that these legendary knights, called the \"Seven Deadly Sins\", were still alive. Ten years later, the Holy Knights themselves staged a coup d’état, and thus became the new, tyrannical rulers of the Kingdom of Liones.",
    ),
  ];
  List<Book> get items {
    return [...dummyBook];
  }

  Book findById(String id) {
    return dummyBook.firstWhere((pdt) => pdt.id == id);
  }
}
