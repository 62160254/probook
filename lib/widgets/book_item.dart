import 'package:flutter/material.dart';
import 'package:bookshop_porject/screen/book_detail.dart';
import 'package:provider/provider.dart';
import '../model/book.dart';
import '../model/cart.dart';

class PdtItem extends StatelessWidget {
  final String name;
  final String imageUrl;

  PdtItem({required this.name, required this.imageUrl});
  @override
  Widget build(BuildContext context) {
    final pdt = Provider.of<Book>(context);
    final cart = Provider.of<Cart>(context);
    return GestureDetector(
      onTap: () {
        Navigator.of(context)
            .pushNamed(BookDetail.routeName, arguments: pdt.id);
      },
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: GridTile(
          child: Image.network(imageUrl),
          footer: GridTileBar(
            title: Text(name),
            trailing: IconButton(
                icon: const Icon(
                  Icons.shopping_cart,
                  color: Colors.black87,
                  size: 25.0,
                ),
                onPressed: () {
                  cart.addItem(pdt.id, pdt.name, pdt.price);
                }),
            backgroundColor: Colors.black87,
          ),
        ),
      ),
    );
  }
}
