import 'package:bookshop_porject/model/book.dart';
import 'package:bookshop_porject/model/cart.dart';
import 'package:bookshop_porject/screen/MyhomePage.dart';
import 'package:bookshop_porject/screen/book_detail.dart';
import 'package:bookshop_porject/screen/cart_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(
          value: Products(),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Anime MangaStore',
        theme: ThemeData(visualDensity: VisualDensity.adaptivePlatformDensity),
        home: MyHomePage(),
        routes: {CartScreen.routeName: (ctx) => CartScreen()},
      ),
    );
  }
}
